package com.auth.server.repository;

import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

import com.auth.server.entity.User;

@Repository
public interface UserRepository extends MongoRepository<User, Long> {

	User findByUserName(String userName);

}
