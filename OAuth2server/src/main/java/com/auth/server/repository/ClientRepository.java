package com.auth.server.repository;


import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

import com.auth.server.entity.AppClient;


@Repository
public interface ClientRepository extends MongoRepository<AppClient, Long> {

	AppClient findByClientId(String clientId);

}
