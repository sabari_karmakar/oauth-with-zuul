package com.auth.server.entity;

import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.data.mongodb.core.mapping.Field;

@Document("AppClient")
public class AppClient {

	@Field("id")
	private Long id;

	@Field("clientId")
	private String clientId;

	
	private String client_secret;

	
	private int access_token_validity;

	
	private String scope;

	
	private String authorities;

	
	private String authorized_grant_types;

	
	private int refresh_token_validity;

	
	private String resource_ids;

	
	private String web_server_redirect_uri;

	
	private String auto_approve;

	
	private String additional_information;

	public AppClient() {
		super();
	}

	public AppClient(String clientId, String client_secret, int access_token_validity, String scope,
			String authorities, String authorized_grant_types, int refresh_token_validity, String resource_ids) {
		super();
		this.clientId = clientId;
		this.client_secret = client_secret;
		this.access_token_validity = access_token_validity;
		this.scope = scope;
		this.authorities = authorities;
		this.authorized_grant_types = authorized_grant_types;
		this.refresh_token_validity = refresh_token_validity;
		this.resource_ids = resource_ids;
	}

	public AppClient(String clientId, String client_secret, int access_token_validity, String scope,
			String authorities, String authorized_grant_types, int refresh_token_validity, String resource_ids,
			String redirectUri) {
		super();
		this.clientId = clientId;
		this.client_secret = client_secret;
		this.access_token_validity = access_token_validity;
		this.scope = scope;
		this.authorities = authorities;
		this.authorized_grant_types = authorized_grant_types;
		this.refresh_token_validity = refresh_token_validity;
		this.resource_ids = resource_ids;
		this.web_server_redirect_uri = redirectUri;
	}

	public Long getId() {
		return id;
	}

	public void set_id(Long id) {
		this.id = id;
	}

	public String getClientId() {
		return clientId;
	}

	public void setClientId(String clientId) {
		this.clientId = clientId;
	}

	public String getClient_secret() {
		return client_secret;
	}

	public void setClient_secret(String client_secret) {
		this.client_secret = client_secret;
	}

	public int getAccess_token_validity() {
		return access_token_validity;
	}

	public void setAccess_token_validity(int access_token_validity) {
		this.access_token_validity = access_token_validity;
	}

	public String getScope() {
		return scope;
	}

	public void setScope(String scope) {
		this.scope = scope;
	}

	public String getAuthorities() {
		return authorities;
	}

	public void setAuthorities(String authorities) {
		this.authorities = authorities;
	}

	public String getAuthorized_grant_types() {
		return authorized_grant_types;
	}

	public void setAuthorized_grant_types(String authorized_grant_types) {
		this.authorized_grant_types = authorized_grant_types;
	}

	public int getRefresh_token_validity() {
		return refresh_token_validity;
	}

	public void setRefresh_token_validity(int refresh_token_validity) {
		this.refresh_token_validity = refresh_token_validity;
	}

	public String getResource_ids() {
		return resource_ids;
	}

	public void setResource_ids(String resource_ids) {
		this.resource_ids = resource_ids;
	}

	public String getWeb_server_redirect_uri() {
		return web_server_redirect_uri;
	}

	public void setWeb_server_redirect_uri(String web_server_redirect_uri) {
		this.web_server_redirect_uri = web_server_redirect_uri;
	}

	public String getAuto_approve() {
		return auto_approve;
	}

	public void setAuto_approve(String auto_approve) {
		this.auto_approve = auto_approve;
	}

	public String getAdditional_information() {
		return additional_information;
	}

	public void setAdditional_information(String additional_information) {
		this.additional_information = additional_information;
	}

	public String getWebServerRedirectUri() {
		// TODO Auto-generated method stub
		return null;
	}

}
