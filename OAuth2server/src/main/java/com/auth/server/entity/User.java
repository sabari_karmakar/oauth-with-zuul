package com.auth.server.entity;

import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.data.mongodb.core.mapping.Field;

@Document("User")
public class User{

	
	@Field("id")
	private Long id;

	@Field
	private String userName;

	@Field
	private String password;

	@Field
	private String user_type;

	
	private String email;

	
	private String role;

	
	private Integer contact_number;

	
	private Integer rating;

	
	
	public User() {
		super();
	}

	public User(Long id, String userName, String user_type, String password) {
		this.id = id;
		this.userName = userName;
		this.password = password;
		

	}

	public User(String userName, String user_type, String password) {

		this.userName = userName;
		this.password = password;
		
		this.contact_number = 54323;
		this.rating = 0;
	}

	public User(User user) {
		super();
		this.id = user.getId();
		this.userName = user.getUserName();
		this.password = user.getPassword();
		this.user_type = user.getUser_type();
		this.email = user.getEmail();
		this.contact_number = user.getContact_number();
		this.rating = user.getRating();
		this.role = user.getRole();
	}

	public User(String userName) {
		this.userName = userName;
		this.id = 101L;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getUser_type() {
		return user_type;
	}

	public void setUser_type(String user_type) {
		this.user_type = user_type;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getRole() {
		return role;
	}

	public void setRole(String role) {
		this.role = role;
	}


	public Integer getContact_number() {
		return contact_number;
	}

	public void setContact_number(Integer contact_number) {
		this.contact_number = contact_number;
	}

	public Integer getRating() {
		return rating;
	}

	public void setRating(Integer rating) {
		this.rating = rating;
	}

	

	

}
